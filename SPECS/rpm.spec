# WARNING: This package is synced with FC

%define lib64arches	x86_64 

%ifarch %lib64arches
    %define _lib lib64
%else
    %define _lib lib
%endif

%define _prefix /usr
%define _libdir %_prefix/%_lib
%define _bindir %_prefix/bin
%define _sysconfdir /etc
%define _datadir /usr/share
%define _defaultdocdir %_datadir/doc
%define _localstatedir /var
%define _infodir %_datadir/info

%if %{?mklibname:0}%{?!mklibname:1}
%define mklibname(ds)  %{_lib}%{1}%{?2:%{2}}%{?3:_%{3}}%{-s:-static}%{-d:-devel}
%endif

%if %{?distsuffix:0}%{?!distsuffix:1}
%define distsuffix .mga
%endif

%if %{?_real_vendor:0}%{?!_real_vendor:1}
%define _real_vendor mageia
%endif

%if %{?mkrel:0}%{?!mkrel:1}
%define mkrel(c:) %{-c: 0.%{-c*}.}%{1}%{?distsuffix:%distsuffix}%{?!distsuffix:.mga}%{?mageia_release:%mageia_release}%{?subrel:.%subrel}
%endif

%if %{?pyver:0}%{?!pyver:1}
%define pyver %(python -V 2>&1 | cut -f2 -d" " | cut -f1,2 -d".")
%endif

%define __find_requires %{rpmhome}/%{_real_vendor}/find-requires %{?buildroot:%{buildroot}} %{?_target_cpu:%{_target_cpu}}
%define __find_provides %{rpmhome}/%{_real_vendor}/find-provides

# run internal testsuite?
%bcond_without check
# build with new db format
%bcond_with ndb
%bcond_with debug

# Define directory which holds rpm config files, and some binaries actually
# NOTE: it remains */lib even on lib64 platforms as only one version
#       of rpm is supported anyway, per architecture
%define rpmhome /usr/lib/rpm

%global rpmver 4.13.0
#global snapver		rc2
%global srcver %{version}%{?snapver:-%{snapver}}
%global srcdir %{?snapver:testing}%{!?snapver:%{name}-%(v=%{version}; echo ${v%.*}.x)}
%global libmajor	7
%global librpmname      %mklibname rpm  %{libmajor}
%global librpmnamedevel %mklibname -d rpm
%global librpmsign      %mklibname rpmsign %{libmajor}
%global librpmbuild     %mklibname rpmbuild %{libmajor}

%global rpmsetup_version 1.34

Summary:	The RPM package management system
Name:		rpm
Epoch:		1
Version:        %{rpmver}
Release:	%mkrel %{?snapver:0.%{snapver}.}8
Group:		System/Packaging
#Source:		http://www.rpm.org/releases/rpm-%{libver}.x/rpm-%{srcver}.tar.bz2
Source0:	http://rpm.org/releases/%{srcdir}/%{name}-%{srcver}.tar.bz2
# extracted from http://pkgs.fedoraproject.org/cgit/redhat-rpm-config.git/plain/macros:
Source1:	macros.filter

#
# Fedora patches
#

# gnupg2 comes installed by default, avoid need to drag in gnupg too
Patch4: rpm-4.8.1-use-gpg2.patch

# Patches already upstream:
#Patch100: rpm-4.13.x-transfiletriggerpostun-invalid-read.patch
#Patch101: rpm-4.13.0-signaturesize.patch
#Patch102: rpm-4.13.0-gdbindex.patch

# These are not yet upstream
Patch302: rpm-4.7.1-geode-i686.patch
# Probably to be upstreamed in slightly different form
Patch304: rpm-4.9.1.1-ld-flags.patch

#
# End of FC patches
# 

#
# Upstream patches not carried by FC:
#
#Patch501: 0001-rpm2cpio.sh-refactoring-to-reduce-extra-dependencies.patch
# Automatically handle ruby gem extraction in %%setup:
#Patch502: 0001-Add-RubyGems-support.patch
# fix testsuite:
#Patch503: 0001-Fix-error-handling-in-rpmio-Python-binding-test-case.patch
#Patch505: 0001-fix-testsuite-adjust-pkg-list.patch
# file triggers fixes:
#Patch600: 0001-Require-whitespace-around-trigger-separators-RhBug-1.patch
#Patch601: 0002-Revise-trigger-separator-checking-a-bit-to-pave-way-.patch
#Patch602: 0003-Ensure-that-a-trigger-condition-actually-exists.patch
#Patch603: 0004-Ensure-that-file-trigger-conditions-are-paths.patch
#Patch604: 0005-Make-package-exists-does-not-exist-error-more-inform.patch
#
# Mageia patches
#

# In original rpm, -bb --short-circuit does not work and run all stage
# From popular request, we allow to do this
# http://qa.mandriva.com/show_bug.cgi?id=15896
Patch70:	rpm-4.12.90-bb-shortcircuit.patch

# don't conflict for doc files
# (to be able to install lib*-devel together with lib64*-devel even if they have conflicting manpages)
Patch83: rpm-4.12.0-no-doc-conflicts.patch

# Fix http://qa.mandriva.com/show_bug.cgi?id=19392
# (is this working??)
Patch84: rpm-4.4.2.2-rpmqv-ghost.patch

# [Dec 2008] macrofiles from rpmrc does not overrides MACROFILES anymore
# Upstream 4.11 will have /usr/lib/rpm/macros.d:
Patch144: rpm-4.9.0-read-macros_d-dot-macros.patch

# without this patch, "#%%define foo bar" is surprisingly equivalent to "%%define foo bar"
# with this patch, "#%%define foo bar" is a fatal error
# Bug still valid => Send upstream for review.
Patch145: rpm-forbid-badly-commented-define-in-spec.patch

# (nb: see the patch for more info about this issue)
#Patch151: rpm-4.6.0-rc1-protect-against-non-robust-futex.patch

# Introduce (deprecated) %%apply_patches:
# (To be dropped once all pkgs are converted to %%auto_setup)
Patch157: rpm-4.10.1-introduce-_after_setup-which-is-called-after-setup.patch
Patch159: introduce-apply_patches-and-lua-var-patches_num.patch

#
# Merge mageia's perl.prov improvements back into upstream:
#
# making sure automatic provides & requires for perl package are using the new
# macro %%perl_convert_version:
Patch162: use_perl_convert_version.diff

#
# Merge mageia's find-requires.sh improvements back into upstream:
#
# (tv) output perl-base requires instead of /usr/bin/perl with internal generator:
# (ngompa) This patch can be dropped once we switch fully over to dnf
Patch170: script-perl-base.diff
# (tv) do not emit requires for /bin/sh (required by glibc) or interpreters for which
# we have custom
Patch172: script-filtering.diff
# (tv) "resolve" /bin/env foo interpreter to actual path, rather than generating
# dependencies on coreutils, should trim off ~800 dependencies more
Patch173: script-env.diff
# (tv) output pkgconfig requires instead of /usr/bin/pkgconfig with internal generator:
# (ngompa) This patch can be dropped once we switch fully over to dnf
Patch174: pkgconfig.diff
# (tv) no not emit "rtld(GNU_HASH)" requires as we've support for it since mga1:
# (saves ~5K packages' dependency in synthesis)
Patch175: no-rtld_GNU_HASH_req.diff
# (tv) replace file deps by requires on packages (when interp is installed):
# (ngompa) This patch can be dropped once we switch fully over to dnf
Patch176: script-no-file-deps.diff
# (tv) replace file deps by requires on packages (common cases for !BRed interp):
# (ngompa) This patch can be dropped once we switch fully over to dnf
Patch177: script-no-file-deps2.diff
# (pt) generate ELF provides for libraries, not only for executables
Patch180: elf_libs_req.diff 
# [Suse]add --assumeexec option for previous patch:
Patch181: assumeexec.diff 
# (Martin Whitaker) disable the systemd-inhibit plugin when systemd-logind is not running (mga#20016):
Patch182: systemd-inhibit-requires-logind.patch

# Various arch enabling:
Patch3003: rpm_arm_mips_isa_macros.patch
Patch3004: rpm_add_armv5tl.patch


# Mageia patches that are easier to rediff on top of FC patches:
#---------------------------------------------------------------
# (tv) merge mga stuff from rpm-setup:
# (for spec-helper)
Patch4000: rpm-4.10.0-find-debuginfo__mga-cfg.diff

# 2 patches to drop in mga7:
# (tv) make old suggests be equivalent to recommends (RECOMMENDNAME -> OLDSUGGEST):
Patch4010: rpm-4.12.0-oldsuggest_equals_recommends.patch
# (tv) uneeeded: maps RECOMMENDNEVR to OLDSUGGEST instead of OLDRECOMMEND
Patch4012: rpm-mga-suggests.diff

# (Debian): avoid useless) linking (&dependency) on all supported python versions:
Patch6001: do-not-link-libpython.patch

# Partially GPL/LGPL dual-licensed and some bits with BSD
# SourceLicense: (GPLv2+ and LGPLv2+ with exceptions) and BSD 
License: GPLv2+

BuildRequires:	autoconf
BuildRequires:	pkgconfig(zlib)
BuildRequires:  bzip2-devel
BuildRequires:	pkgconfig(liblzma) >= 5
BuildRequires:	automake
BuildRequires:	doxygen
BuildRequires:	elfutils-devel
BuildRequires:	libbeecrypt-devel
BuildRequires:	binutils-devel
BuildRequires:	ed
BuildRequires:	gettext-devel
BuildRequires:  db5.3-devel
BuildRequires:  pkgconfig(dbus-1)
BuildRequires:  pkgconfig(neon)
BuildRequires:	pkgconfig(popt)
BuildRequires:	pkgconfig(nss)
BuildRequires:	magic-devel
BuildRequires:  rpm-%{_real_vendor}-setup-build %{?rpmsetup_version:>= %{rpmsetup_version}}
BuildRequires:  readline-devel
BuildRequires:	pkgconfig(ncurses)
BuildRequires:  pkgconfig(libssl)
BuildRequires:  pkgconfig(lua) >= 5.2.3-3.mga5
BuildRequires:  pkgconfig(libcap)
BuildRequires:  libacl-devel
BuildRequires:  pkgconfig(libarchive)
BuildRequires:  pkgconfig(python)
BuildRequires:  pkgconfig(python-3.5)
# for testsuite:
BuildRequires: eatmydata
%if %{with check}
BuildRequires: fakechroot
%endif

Requires:	bzip2 >= 0.9.0c-2
Requires:	xz
Requires:	cpio
Requires:	gawk
Requires:	mktemp
Requires:	setup >= 2.2.0-8
Requires:	rpm-%{_real_vendor}-setup >= 1.85
Requires:	update-alternatives
Requires:	%librpmname = %epoch:%version-%release
URL:            http://rpm.org/
%define         git_url        http://rpm.org/git/rpm.git
Requires(pre):		rpm-helper
Requires(pre):		coreutils
Requires(postun):	rpm-helper

# fix for plugins conflict:
Conflicts: %{_lib}rpm3 < 1:4.12.0.1-20.3

%description
The RPM Package Manager (RPM) is a powerful command line driven
package management system capable of installing, uninstalling,
verifying, querying, and updating software packages.  Each software
package consists of an archive of files along with information about
the package like its version, a description, etc.

%package -n %librpmname
Summary:  Libraries for manipulating RPM packages
Group:	  System/Libraries
License: GPLv2+ and LGPLv2+ with exceptions
Provides: librpm = %version-%release
Provides: rpm-libs = %version-%release

%description -n %librpmname
This package contains the RPM shared libraries.

%package   -n %librpmbuild
Summary:   Libraries for building and signing RPM packages
Group:     System/Libraries
License: GPLv2+ and LGPLv2+ with exceptions
Obsoletes: rpm-build-libs%{_isa} < %{version}-%{release}
Provides: rpm-build-libs%{_isa} = %{version}-%{release}

%description -n %librpmbuild
This package contains the RPM shared libraries for building and signing
packages.

%package -n %librpmnamedevel
Summary:	Development files for applications which will manipulate RPM packages
Group:		Development/C
License: GPLv2+ and LGPLv2+ with exceptions
Requires:	rpm = %epoch:%{version}-%{release}
Provides:	librpm-devel = %version-%release
Provides:   	rpm-devel = %version-%release
Requires:       %librpmname = %epoch:%version-%release
Requires:       %librpmbuild = %epoch:%version-%release
Requires:       %librpmsign = %epoch:%version-%release

%description -n %librpmnamedevel
This package contains the RPM C library and header files.  These
development files will simplify the process of writing programs that
manipulate RPM packages and databases. These files are intended to
simplify the process of creating graphical package managers or any
other tools that need an intimate knowledge of RPM packages in order
to function.

This package should be installed if you want to develop programs that
will manipulate RPM packages and databases.

%package  -n %librpmsign
Summary:  Libraries for building and signing RPM packages
Group:    System/Libraries
License: GPLv2+ and LGPLv2+ with exceptions

%description -n %librpmsign
This package contains the RPM shared libraries for building and signing
packages.

%package build
Summary:	Scripts and executable programs used to build packages
Group:		System/Packaging
Requires:	autoconf
Requires:	automake
Requires:	file
Requires:	gcc-c++
# We need cputoolize & amd64-* alias to x86_64-* in config.sub
Requires:	libtool-base
Requires:	patch
Requires:	make
Requires:	tar
Requires:	unzip
# Versioned requirement for Patch 400
Requires:	elfutils >= 0.167-2
Requires:	perl(CPAN::Meta) >= 2.112.150
Requires:	perl(ExtUtils::MakeMaker) >= 6.570_700
Requires:       perl(YAML::Tiny)
Requires:	rpm = %epoch:%{version}-%{release}
Requires:	rpm-%{_real_vendor}-setup-build %{?rpmsetup_version:>= %{rpmsetup_version}}
Requires:	%librpmbuild = %epoch:%version

%description build
The rpm-build package contains the scripts and executable programs
that are used to build packages using the RPM Package Manager.

%package sign
Summary: Package signing support
Group:   System/Base

%description sign
This package contains support for digitally signing RPM packages.

%package -n python2-%{name}
Summary:	Python 2 bindings for apps which will manipulate RPM packages
Group:		Development/Python
Requires:	rpm = %epoch:%{version}-%{release}
Provides: python-%{name} = %epoch:%{version}-%{release}
Obsoletes: python-%{name} <= 1:4.13.0-3

%description -n python2-%{name}
The python2-rpm package contains a module that permits applications
written in the Python programming language to use the interface
supplied by RPM Package Manager libraries.

This package should be installed if you want to develop Python 2
programs that will manipulate RPM packages and databases.

%package -n python3-%{name}
Summary:	Python 3 bindings for apps which will manipulate RPM packages
Group:		Development/Python
Requires:	rpm = %epoch:%{version}-%{release}

%description -n python3-%{name}
The python3-rpm package contains a module that permits applications
written in the Python programming language to use the interface
supplied by RPM Package Manager libraries.

This package should be installed if you want to develop Python 3
programs that will manipulate RPM packages and databases.

%package apidocs
Summary: API documentation for RPM libraries
Group:   Documentation
BuildArch: noarch
# temporary cauldron rename:
%rename rpm-doc

%description apidocs
This package contains API documentation for developing applications
that will manipulate RPM packages and databases.

%prep
%autosetup -n %{name}-%{srcver} 1} -p1

%build
%define _disable_ld_no_undefined 1
aclocal
automake-1.14 --add-missing
automake
autoreconf

%if %with debug
RPM_OPT_FLAGS=-g
%endif
export CPPFLAGS="$CPPFLAGS `pkg-config --cflags nss`"
CFLAGS="$RPM_OPT_FLAGS -fPIC" CXXFLAGS="$RPM_OPT_FLAGS -fPIC" \
%configure2_5x \
    --localstatedir=%{_var} \
    --sharedstatedir=%{_var}/lib \
    %{?_with_debug} \
    --with-external-db \
    --with-lua \
    --without-selinux \
    --with-cap \
    --with-acl \
    %{?with_ndb: --with-ndb} \
    --enable-python

%make_build
pushd python
%{__python2} setup.py build
%{__python3} setup.py build
popd

%install
%make_install

# We need to build with --enable-python for the self-test suite, but we
# actually package the bindings built with setup.py (#531543#c26)
rm -rf $RPM_BUILD_ROOT/%{python_sitearch}
pushd python
%{__python2} setup.py install --skip-build --root $RPM_BUILD_ROOT
%{__python3} setup.py install --skip-build --root $RPM_BUILD_ROOT
popd

# Save list of packages through cron
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/cron.daily
install -m 755 scripts/rpm.daily ${RPM_BUILD_ROOT}%{_sysconfdir}/cron.daily/rpm

mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d
install -m 644 scripts/rpm.log ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/rpm

mkdir -p $RPM_BUILD_ROOT/var/lib/rpm
for dbi in \
	Basenames Conflictname Dirnames Group Installtid Name Obsoletename \
	Packages Providename Requirename Triggername Sha1header Sigmd5 \
	__db.001 __db.002 __db.003 __db.004 __db.005 __db.006 __db.007 \
	__db.008 __db.009
do
    touch $RPM_BUILD_ROOT/var/lib/rpm/$dbi
done

test -d doc-copy || mkdir doc-copy
rm -rf doc-copy/*
ln -f doc/manual/* doc-copy/
rm -f doc-copy/Makefile*

mkdir -p $RPM_BUILD_ROOT/var/spool/repackage

mkdir -p %buildroot%rpmhome/macros.d
install %SOURCE1 %buildroot%rpmhome/macros.d
mkdir -p %buildroot%_sysconfdir/rpm/macros.d
cat > %buildroot%_sysconfdir/rpm/macros <<EOF
# Put your own system macros here
# usually contains 

# Set this one according your locales
# %%_install_langs

EOF

%{rpmhome}/%{_host_vendor}/find-lang.pl $RPM_BUILD_ROOT %{name}

find $RPM_BUILD_ROOT -name "*.la"|xargs rm -f

%if %{with check}
%check
eatmydata make check
[ "$(ls -A tests/rpmtests.dir)" ] && cat tests/rpmtests.log
%endif

%pre
/usr/share/rpm-helper/add-user rpm $1 rpm /var/lib/rpm /bin/false

rm -rf /usr/lib/rpm/*-mandrake-*
rm -rf /usr/lib/rpm/*-%{_real_vendor}-*


%post
# nuke __db.00? when updating to this rpm
rm -f /var/lib/rpm/__db.00?

if [ ! -f /var/lib/rpm/Packages ]; then
    /bin/rpm --initdb
fi

%postun
/usr/share/rpm-helper/del-user rpm $1 rpm

%define	rpmattr		%attr(0755, rpm, rpm)

%files -f %{name}.lang
%license COPYING
%doc CHANGES doc/manual/[a-z]*
%attr(0755,rpm,rpm) /bin/rpm
%attr(0755, rpm, rpm) %{_bindir}/rpm2cpio
%attr(0755, rpm, rpm) %{_bindir}/rpm2archive
%attr(0755, rpm, rpm) %{_bindir}/gendiff
%attr(0755, rpm, rpm) %{_bindir}/rpmdb
%attr(0755, rpm, rpm) %{_bindir}/rpmkeys
%attr(0755, rpm, rpm) %{_bindir}/rpmgraph
%{_bindir}/rpmquery
%{_bindir}/rpmverify
%{_libdir}/rpm-plugins

%dir %{_localstatedir}/spool/repackage
%dir %{rpmhome}
%dir /etc/rpm
%config(noreplace) /etc/rpm/macros
%dir /etc/rpm/macros.d
%attr(0755, rpm, rpm) %{rpmhome}/config.guess
%attr(0755, rpm, rpm) %{rpmhome}/config.sub
%attr(0755, rpm, rpm) %{rpmhome}/rpmdb_*
%attr(0644, rpm, rpm) %{rpmhome}/macros
%rpmhome/macros.d
%attr(0755, rpm, rpm) %{rpmhome}/mkinstalldirs
%attr(0755, rpm, rpm) %{rpmhome}/rpm.*
%attr(0644, rpm, rpm) %{rpmhome}/rpmpopt*
%attr(0644, rpm, rpm) %{rpmhome}/rpmrc
%attr(0755, rpm, rpm) %{rpmhome}/elfdeps
%attr(0755, rpm, rpm) %{rpmhome}/script.req

%rpmattr	%{rpmhome}/rpm2cpio.sh
%rpmattr	%{rpmhome}/tgpg

%dir %attr(   -, rpm, rpm) %{rpmhome}/fileattrs
%attr(0644, rpm, rpm) %{rpmhome}/fileattrs/*.attr

%dir %attr(   -, rpm, rpm) %{rpmhome}/platform/
%exclude %{rpmhome}/platform/m68k-linux/macros
%exclude %{rpmhome}/platform/riscv64-linux/macros
%ifarch %{ix86} x86_64
%attr(   -, rpm, rpm) %{rpmhome}/platform/i*86-*
%attr(   -, rpm, rpm) %{rpmhome}/platform/athlon-*
%attr(   -, rpm, rpm) %{rpmhome}/platform/pentium*-*
%attr(   -, rpm, rpm) %{rpmhome}/platform/geode-*
%else
%exclude %{rpmhome}/platform/i*86-linux/macros
%exclude %{rpmhome}/platform/athlon-linux/macros
%exclude %{rpmhome}/platform/pentium*-linux/macros
%exclude %{rpmhome}/platform/geode-linux/macros
%endif
%ifarch x86_64
%attr(   -, rpm, rpm) %{rpmhome}/platform/amd64-*
%attr(   -, rpm, rpm) %{rpmhome}/platform/x86_64-*
%attr(   -, rpm, rpm) %{rpmhome}/platform/ia32e-*
%else
%exclude %{rpmhome}/platform/amd64-linux/macros
%exclude %{rpmhome}/platform/ia32e-linux/macros
%exclude %{rpmhome}/platform/x86_64-linux/macros
%endif
%ifarch %arm
%attr(   -, rpm, rpm) %{rpmhome}/platform/arm*
%attr(   -, rpm, rpm) %{rpmhome}/platform/aarch64*/macros
%else
%exclude %{rpmhome}/platform/arm*/macros
%exclude %{rpmhome}/platform/aarch64*/macros
%endif
%attr(   -, rpm, rpm) %{rpmhome}/platform/noarch*
# new in 4.10.0:
%exclude %{rpmhome}/platform/alpha*-linux/macros
%exclude %{rpmhome}/platform/sparc*-linux/macros
%exclude %{rpmhome}/platform/ia64*-linux/macros
%exclude %{rpmhome}/platform/m68k*-linux/macros
%exclude %{rpmhome}/platform/mips*-linux/macros
%exclude %{rpmhome}/platform/ppc*-linux/macros
%exclude %{rpmhome}/platform/s390*-linux/macros
%exclude %{rpmhome}/platform/sh*-linux/macros



%{_mandir}/man8/rpm.8*
%{_mandir}/man8/rpmdb.8*
%{_mandir}/man8/rpmgraph.8*
%{_mandir}/man8/rpmkeys.8*
%{_mandir}/man8/rpm2cpio.8*
%{_mandir}/man1/*.1*
%lang(fr) %{_mandir}/fr/man[18]/*.[18]*
%lang(ja) %{_mandir}/ja/man[18]/*.[18]*
%lang(ko) %{_mandir}/ko/man[18]/*.[18]*
%lang(pl) %{_mandir}/pl/man[18]/*.[18]*
%lang(ru) %{_mandir}/ru/man[18]/*.[18]*
%lang(sk) %{_mandir}/sk/man[18]/*.[18]*

%config(noreplace,missingok)	/etc/cron.daily/rpm
%config(noreplace,missingok)	/etc/logrotate.d/rpm

%attr(0755, rpm, rpm)	%dir %_localstatedir/lib/rpm

%define	rpmdbattr %attr(0644, rpm, rpm) %verify(not md5 size mtime) %ghost %config(missingok,noreplace)

%rpmdbattr	/var/lib/rpm/Basenames
%rpmdbattr	/var/lib/rpm/Conflictname
%rpmdbattr	/var/lib/rpm/__db.0*
%rpmdbattr	/var/lib/rpm/Dirnames
%rpmdbattr	/var/lib/rpm/Group
%rpmdbattr	/var/lib/rpm/Installtid
%rpmdbattr	/var/lib/rpm/Name
%rpmdbattr	/var/lib/rpm/Obsoletename
%rpmdbattr	/var/lib/rpm/Packages
%rpmdbattr	/var/lib/rpm/Providename
%rpmdbattr	/var/lib/rpm/Provideversion
%rpmdbattr	/var/lib/rpm/Removetid
%rpmdbattr	/var/lib/rpm/Requirename
%rpmdbattr	/var/lib/rpm/Requireversion
%rpmdbattr	/var/lib/rpm/Sha1header
%rpmdbattr	/var/lib/rpm/Sigmd5
%rpmdbattr	/var/lib/rpm/Triggername

%files -n %librpmname
%{_libdir}/librpm.so.%{libmajor}*
%{_libdir}/librpmio.so.%{libmajor}*

%files -n %librpmbuild
%{_libdir}/librpmbuild.so.%{libmajor}*

%files -n %librpmsign
%{_libdir}/librpmsign.so.%{libmajor}*

%files build
%doc CHANGES
%doc doc-copy/*
%rpmattr	%{_bindir}/rpmbuild
%rpmattr        %{_bindir}/rpmspec
%rpmattr	%{_prefix}/lib/rpm/brp-*
%rpmattr	%{_prefix}/lib/rpm/check-files
%rpmattr	%{_prefix}/lib/rpm/debugedit
%rpmattr	%{_prefix}/lib/rpm/sepdebugcrcfix
%rpmattr	%{_prefix}/lib/rpm/*.prov 
%rpmattr	%{_prefix}/lib/rpm/find-debuginfo.sh
%rpmattr	%{_prefix}/lib/rpm/find-lang.sh
%rpmattr	%{_prefix}/lib/rpm/find-provides
%rpmattr	%{_prefix}/lib/rpm/find-requires
%rpmattr	%{_prefix}/lib/rpm/perl.req

%rpmattr	%{_prefix}/lib/rpm/check-buildroot
%rpmattr	%{_prefix}/lib/rpm/check-prereqs
%rpmattr	%{_prefix}/lib/rpm/check-rpaths
%rpmattr	%{_prefix}/lib/rpm/check-rpaths-worker
%rpmattr	%{_prefix}/lib/rpm/libtooldeps.sh
%rpmattr	%{_prefix}/lib/rpm/macros.perl
%rpmattr	%{_prefix}/lib/rpm/macros.php
%rpmattr	%{_prefix}/lib/rpm/macros.python
%rpmattr	%{_prefix}/lib/rpm/mono-find-provides
%rpmattr	%{_prefix}/lib/rpm/mono-find-requires
%rpmattr	%{_prefix}/lib/rpm/ocaml-find-provides.sh
%rpmattr	%{_prefix}/lib/rpm/ocaml-find-requires.sh
%rpmattr	%{_prefix}/lib/rpm/pkgconfigdeps.sh

%rpmattr	%{_prefix}/lib/rpm/rpmdeps
%rpmattr        %{_prefix}/lib/rpm/pythondeps.sh


%{_mandir}/man8/rpmbuild.8*
%{_mandir}/man8/rpmdeps.8*
%{_mandir}/man8/rpmspec.8*

%files sign
%{_bindir}/rpmsign
%{_mandir}/man8/rpmsign.8*

%files -n python2-%{name}
%{python_sitearch}/%{name}/
%{python_sitearch}/%{name}_python-*.egg-info

%files -n python3-%{name}
%defattr(-,root,root)
%{python3_sitearch}/%{name}/
%{python3_sitearch}/%{name}_python-*.egg-info

%files -n %librpmnamedevel
%{_libdir}/librp*[a-z].so
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/%{name}/

%files apidocs
%license COPYING
%doc doc/librpm/html/*

%changelog
* Wed Jan 25 2017 Thierry Vignaud <tv@mageia.org> 1:4.13.0-8.mga6
+ Revision: 1083416
- Fix malformed packages being generated around 4GB boundary (rhbz#1405570)
- Resurrect debuginfo GDB index generation (rhbz#1410907)

* Thu Dec 29 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-7.mga6
+ Revision: 1078606
- disable the systemd-inhibit plugin when systemd-logind is not running (Martin
  Whitaker, mga#20016)
+ Thomas Backlund <tmb@mageia.org>
- use hardcoded epoch in python-rpm obosolete (Rémi)

* Sun Nov 13 2016 Thomas Backlund <tmb@mageia.org> 1:4.13.0-6.mga6
+ Revision: 1066937
- fix obsoleting python-rpm (missing epoch)

* Fri Nov 11 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-5.mga6
+ Revision: 1066503
- add more check regarding trigger separators

* Fri Nov 11 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-4.mga6
+ Revision: 1066388
- patch 506: Ensure that file trigger conditions are paths (mga#18797)
  (beware glib2.0)
- drop now uneeded tarball of missing files
- drop very old conflicts
- do not include all man8 man pages when some are also in other subpkgs
- move rpmspec man page in the build subpkg that includes it
- sort
- rename python-rpm -> python2-rpm
- drop mga5 specific lua requires

* Mon Nov 07 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-3.mga6
+ Revision: 1065616
- fix invalid memory access on %%transfiletriggerpostun (rhbz#1284645, mga#18797)

* Thu Nov 03 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-2.mga6
+ Revision: 1064997
- use gnupg2 for signing
- add conditional to build with ndb
- build with acl support
- remove non existing --with-apidocs option
- remove non existing --without-javaglue option
- remove non existing --with-sqlite3 option
- remove non existing --with-glob option
- add %%bcond_without check
- remove %%bcond_without python
- fix lib summary+descr
- update devel descr
- fix python-rpm name in descr
- fix python3-rpm name in descr
- explain what RPM means

* Thu Nov 03 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-1.mga6
+ Revision: 1064911
- final release
- drop 1 merged patch
- adjust rpmdb index ghosts to match rpm-4.9+ reality
- patch 504: switch to upstream fix for testsuite

* Fri Oct 21 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc2.2.mga6
+ Revision: 1062889
- do not packge GROUPS (mga#10250)
- re-enable testsuite
- source2: add missing files for testsuite
- patches 503-505: fix testsuite
- switch to %%autosetup
- rename doc subpkg as apidocs
- package license with %%license
- update license

* Thu Oct 20 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc2.1.mga6
+ Revision: 1062738
- rpm 4.13.0~rc2
- fix Source URL
- drop merged patches
- BR binutils-devel again
- use %%global instead of %%define

* Wed Oct 19 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.37.mga6
+ Revision: 1062311
- switch to the upstream gem extraction in %%setup (needs adaptation in rubygems.macros in ruby-RubyGems)
- patch 501: fix rpm2cpio.sh (not rpm2cpio) with locales where it's broken (mga#13631)
- renumber patch 132 as it's not a FC patch and conflicts with resyncing

* Sat Oct 15 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.36.mga6
+ Revision: 1060956
- drop %%old_configure I introduced a couple months ago as all packages using it have been fixed
- we don't need tetex anymore

* Mon Oct 10 2016 Neal Gompa <ngompa@mageia.org> 1:4.13.0-0.rc1.35.mga6
+ Revision: 1060025
- Fix rpm2archive to return 0 on successful conversion
- Prevent find-debuginfo.sh from copying extra sections into .gnu_debugdata (rhbz#1382394)
+ Thierry Vignaud <tv@mageia.org>
- Fix signing with non-ASCII uid keys (rhbz#1243963)
- Use armv7hl isa for all armhfp (armv7h*l) arches (rhbz#1326871)

* Tue May 17 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.33.mga6
+ Revision: 1016514
- Filter unversioned deps if corresponding versioned deps exist (rhbz#678605)

* Wed May 11 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.32.mga6
+ Revision: 1013627
- patch 201: fix a segfault in perl-RPM4 testsuite

* Tue Apr 26 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.31.mga6
+ Revision: 1006325
- Fix sigsegv in stringFormat() (rhbz#1316903)
- Fix reading rpmtd behind its size in formatValue() (rhbz#1316896)

* Sat Apr 16 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.30.mga6
+ Revision: 1002880
- use upstream patch for fuzz settings for %%autopatch/%%autosetup
- add %%{_default_patch_flags} to %%__patch which is used in %%autosetup
- enable --no-backup-if-mismatch by default in %%patch macro (rhbz#884755)
- make creating index records consistent for rich & rich-weak deps (rhbz#1325982)

* Thu Apr 14 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.29.mga6
+ Revision: 1001343
- provides: rpm-libs

* Wed Apr 13 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.28.mga6
+ Revision: 1001095
- add RPMCALLBACK_ELEM_PROGRESS callback type
- fix non-working combination of %%lang and %%doc directive (rhbz#1254483)
- add posix.redirect2null (rhbz#1287918)

* Sun Apr 03 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.27.mga6
+ Revision: 997815
- sync localstatedir value with rpm-mageia-setup one
- change platform sharedstatedir to something more sensible (mga#16944)

* Thu Mar 24 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.26.mga6
+ Revision: 995107
- Add posix.redirect2null (rhbz#1287918)
+ Jani Välimaa <wally@mageia.org>
- fix rpm_add_armv5tl.patch to apply with fuzz=0

* Sun Mar 13 2016 Jani Välimaa <wally@mageia.org> 1:4.13.0-0.rc1.25.mga6
+ Revision: 990197
- add patch from Neal Gompa to use fuzz setting with %%autopatch and %%autosetup
+ Thierry Vignaud <tv@mageia.org>
- add a doc subpackage

* Sun Feb 28 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.24.mga6
+ Revision: 980315
- Fix ExclusiveArch/ExcludeArch for noarch packages (rhbz#1298668)
- Fix dependencies for RemovePathPostfixes (rhbz#1306559)

* Tue Feb 23 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.23.mga6
+ Revision: 977389
- block idle and sleep in the systemd-inhibit plugin (rhbz#1297984)
- remove size limit when expanding macros (rhbz#1301677)

* Thu Jan 28 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.22.mga6
+ Revision: 928464
- fix memleak in file triggers

* Thu Jan 21 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.21.mga6
+ Revision: 926192
- fix %%autosetup not to cause errors during run of rpmspec tool (rhbz#1293687)

* Sun Jan 17 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.20.mga6
+ Revision: 924811
- fix recursive calling of rpmdeps tool (rhbz#1297557)
- add support for missingok file attribute

* Thu Jan 14 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.19.mga6
+ Revision: 923008
- use upstream fix for filetriggers in chroo

* Wed Jan 13 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.18.mga6
+ Revision: 922386
- simplify filetriggers-in-chroot fix

* Sun Jan 10 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.17.mga6
+ Revision: 920950
- fix accessing filetriggers indexes in chroot

* Sat Jan 09 2016 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.16.mga6
+ Revision: 920682
- fix transaction file triggers not run in chroot (mga#17217)
- notice that this package is synced with FC

* Sun Nov 29 2015 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.15.mga6
+ Revision: 907209
- enable to disable file triggers
- switch to pkgconfig() BRs

* Sun Nov 08 2015 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.14.mga6
+ Revision: 898696
- fix crash when parsing corrupted RPM file (rhbz#1273360)
- fix SIGSEGV in case of old unsupported gpg keys (rhbz#1277464)

* Sun Nov 01 2015 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.13.mga6
+ Revision: 897085
- ignore SIGPIPE signals during execucton of scriptlets (rhbz#1264198)

* Wed Oct 28 2015 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.12.mga6
+ Revision: 896184
- sync patches with RH:
  o Fix reading memory right after the end of an allocated area (rhbz#1260248)
  o Add support for various dependencies to rpmdeps tool (rhbz#1247092)
  o If %%_wrong_version_format_terminate_build is 1 then terminate build in case
    that version format is wrong i. e. epoch is not unsigned integer or version
    contains more separators (":", "-"). %%_wrong_version_format_terminate_build
    is 1 by deafault (rhbz#1265700)
- BR dbus-devel for systemd inhibit plugin (mga#16950)

* Thu Oct 08 2015 Philippe Makowski <philippem@mageia.org> 1:4.13.0-0.rc1.11.mga6
+ Revision: 887347
- Rebuild for Python3.5

* Thu Sep 17 2015 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.10.mga6
+ Revision: 880163
- fix new rich dependency syntax
- rename cloberred %%configure as %%old_configure
- make %%autopach abort on missing patch

* Sat Sep 05 2015 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.9.mga6
+ Revision: 873116
- submit to core/release
- drop patch 1008 (let's default to upstream XZ crc choice)

* Wed Sep 02 2015 Thierry Vignaud <tv@mageia.org> 1:4.13.0-0.rc1.8.mga6
+ Revision: 872193
- 4.13.0 C1

* Fri Aug 28 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.90-8.mga6
+ Revision: 870747
- resolve some interpreters even if they're not in basesystem or in BRs
  (mga#16134)
- simplify: do interpreter requires blacklisting only once
- patch 0: sync with git, thus dropping 14 patches
  (6 FC ones + 8 MGA ones)

* Tue Aug 25 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.90-7.mga6
+ Revision: 869311
- patch 7002: allow having several "identical"trans file triggers

* Sun Aug 16 2015 Thomas Backlund <tmb@mageia.org> 1:4.12.90-6.mga6
+ Revision: 865174
- dont obsolete lib(64)rpm* with major 3 as it breaks BS
- adapt lib(64)rpm3 conflict to match mga5 update rpm

* Mon Aug 10 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.90-5.mga6
+ Revision: 862378
- drop patch 111 (o more needed since rpm-4.6)
- fix python 3 support
- drop support for MIPS

* Fri Aug 07 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.90-4.mga6
+ Revision: 861512
- split the XZ patch into 2
- make geode appear as i686 (rhbz#517475)
- add RPM_LD_FLAGS to build environment (rhbz#728974)

* Thu Aug 06 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.90-3.mga6
+ Revision: 861501
- add --filetriggers option to show info about file triggers

* Wed Aug 05 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.90-2.mga6
+ Revision: 861242
- drop patch 22 (no more needed since rpm-4.9+)
  (side effect: %%preun faillure now mean uninstall faillure)
- if globbing of a filename fails, try use the filename without globbing.
  (rhbz#1246743)
- modify rpmIsGlob() to be more precise and compatible with glob().
- don't warn when an escaped macro is in a comment (rhbz#1224660)
- fix macros in comments
- explain some patches
- drop patch 3005 (testsuite now works OK)

* Sat Aug 01 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.90-1.mga6
+ Revision: 860282
- fix mga5 upgrade
- drop old filetriggers implementation now that upstream has one
  (see http://www.rpm.org/wiki/FileTriggers)
- switch from %%apply_patches to %%autopatch
- 4.13 alpha

* Sat Aug 01 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-23.mga6
+ Revision: 860239
- move plugins in main package (preps for 4.13 migration)
- revert r469312 (support for non Linux in spec)

* Thu Jul 02 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-22.mga6
+ Revision: 849634
- simplify using %%bcond_with*
- remove support for not building plugins
- drop patch 17 (was adding a -p option to gendiff that was only used by
  chmouel 15 years ago...)
- patch 135: drop it as upstream says it's bogus
- drop patch64 (we don't support mnb for quite some time and we don't
  even ship a mageia/rpmpopt anyway)
- deprecate %%apply_patches (use %%autopatch instead)
- document some patches

* Sun Jun 28 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-21.mga6
+ Revision: 846625
- fix --excludedocs option (rhbz#1192625)
- pass _find_debuginfo_opts -g to eu-strip for executables (rhbz#1186563)
- fix golang debuginfo packages (#1184221)
- add --whatrecommends and friends (rhbz#1231247)
- drop temporary band aid for early rpm-4.12 regression
  (all packages were rebuild after in mga5)
- drop fs check, we're living in a /usr world for several releases now
- kill patch 2100 (unused since rpm-setup is fixed)
  (another nice side effect of siding with FC/Suse)

* Sat Feb 14 2015 Pascal Terjan <pterjan@mageia.org> 1:4.12.0.1-20.mga5
+ Revision: 814948
- Don't fail the build when file reports to many notes in an ELF

* Fri Feb 06 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-19.mga5
+ Revision: 813738
- disable patch #3504 as it breaks SUID (mga#14691)

* Tue Feb 03 2015 Colin Guthrie <colin@mageia.org> 1:4.12.0.1-18.mga5
+ Revision: 813285
- Drop unnecessary warning mga#14971
+ Olav Vitters <ovitters@mageia.org>
- use upstream patch for python3 import patch

* Sun Jan 25 2015 Olav Vitters <ovitters@mageia.org> 1:4.12.0.1-17.mga5
+ Revision: 812204
- add patch to fix parseSpec with Python3
+ Pascal Terjan <pterjan@mageia.org>
- Fix variable used for source URL

* Fri Jan 23 2015 Olav Vitters <ovitters@mageia.org> 1:4.12.0.1-16.mga5
+ Revision: 811986
- add debian patch to fix linking
- add python3 subpackage

* Mon Jan 19 2015 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-15.mga5
+ Revision: 811456
- enhanced python bindings doc

* Thu Dec 11 2014 David Walser <luigiwalser@mageia.org> 1:4.12.0.1-14.mga5
+ Revision: 802704
- adapt redhat patches from rhel to fix CVE-2013-6435

* Thu Nov 13 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-13.mga5
+ Revision: 796716
- BR binutils-devel
- fix .gnu_debuglink CRC32 after dwz (rhbz#971119)

* Sat Nov 01 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-12.mga5
+ Revision: 795108
- skip ghost files in payload (rhbz#1156497)
- fix size and archice size tag generation on big-endian systems

* Wed Oct 22 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-11.mga5
+ Revision: 792552
- no more need for reverting pthread locking

* Sun Oct 19 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-10.mga5
+ Revision: 791609
- revert using pthread lock for logging (mga#14172)

* Wed Oct 15 2014 Sysadmin Bot <umeabot@mageia.org> 1:4.12.0.1-9.mga5
+ Revision: 748731
- Second Mageia 5 Mass Rebuild

* Sun Oct 12 2014 Thomas Backlund <tmb@mageia.org> 1:4.12.0.1-8.mga5
+ Revision: 738142
- Dont wait for transaction lock within scriptlets (RhBug:1135596)

* Tue Sep 30 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-7.mga5
+ Revision: 732818
- add filtering macros from FC

* Fri Sep 26 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-6.mga5
+ Revision: 724978
- fix parsing some perl package versions

* Fri Sep 26 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-5.mga5
+ Revision: 724819
- fix "Invalid version format (alpha without decimal)" for perl-Software-License
- typo fix (Luc Menut)

* Tue Sep 23 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-4.mga5
+ Revision: 721937
- replace file deps by requires on packages
- blacklist libsafe & libfakeroot like in old days

* Mon Sep 22 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-3.mga5
+ Revision: 718427
- blacklist /usr/bin/ocamlrun file requires (we already have "ocaml(runtime)")

* Thu Sep 18 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0.1-2.mga5
+ Revision: 695866
- temporary band-aid for rpm2cpio whining on payload size mismatch (rhbz#1142949)

* Thu Sep 18 2014 Thomas Backlund <tmb@mageia.org> 1:4.12.0.1-1.mga5
+ Revision: 695010
- update to 4.12.0.1

* Wed Sep 17 2014 Pascal Terjan <pterjan@mageia.org> 1:4.12.0-7.mga5
+ Revision: 693176
- Really rebuild with current version

* Wed Sep 17 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0-6.mga5
+ Revision: 693171
- rebuild with itself now that requires are nicely computed

* Wed Sep 17 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0-5.mga5
+ Revision: 693154
- really fix library provides/requires

* Tue Sep 16 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0-4.mga5
+ Revision: 691307
- better fix for library require (w/o debuginfo requires)
+ Sysadmin Bot <umeabot@mageia.org>
- Mageia 5 Mass Rebuild

* Tue Sep 16 2014 Sysadmin Bot <umeabot@mageia.org> 1:4.12.0-2.mga5
+ Revision: 687014
- Rebuild to fix library dependencies

* Tue Sep 16 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0-1.mga5
+ Revision: 686484
- final release
- drop merged patches 4011 & 4013

* Tue Sep 16 2014 Pascal Terjan <pterjan@mageia.org> 1:4.12.0-0.rc1.6.mga5
+ Revision: 685056
- Generate ELF provides for libraries, not only for executables

* Sun Sep 14 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0-0.rc1.5.mga5
+ Revision: 675467
+ rebuild (emptylog)

* Sun Sep 14 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0-0.rc1.4.mga5
+ Revision: 675445
- fix requires

* Sun Sep 14 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0-0.rc1.3.mga5
+ Revision: 675418
- make sure rpm-build pulls the right librpmbuild

* Sun Sep 14 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0-0.rc1.2.mga5
+ Revision: 675243
- filter /bin/bash too (regression when switching to upstream script)
- use %%makeinstall_std

* Sat Sep 13 2014 Thierry Vignaud <tv@mageia.org> 1:4.12.0-0.rc1.1.mga5
+ Revision: 674945
- fix self-provides
- better solution for RECOMMENDS with fallback on OLDSUGGEST:
  add RPMTAG_RECOMMENDNAME to extended tags
- resurrect payload and tilde rpmlib() dependencies
- new release (4.12.0 RC1):
  o all libraries now have the same major
  o BR pkgconfig(libarchive)
  o new tool: rpm2archive
- build plugins
- rediff patches 22, 83, 135, 146, 1007, 3005
- drop patch 31 (syslog support is now a plugin)
- drop patches 133 & 134, weak deps are now implemented upstream:
  (new weak deps implementation, the 3rd one)
  (old tags are renamed: SUGGESTS -> OLDSUGGEST)
  (new tags have different names: SUGGEST)
- patch 4010: make Recommends tag fallback to old suggests
  as it recommends was not used in mdv/mga, we were using SUGGESTS
  in RECOMMENDS place

* Fri Sep 12 2014 Thierry Vignaud <tv@mageia.org> 1:4.11.3-2.mga5
+ Revision: 674891
- prepare switching from "external" to "internal" deps generator:
  o merge some mageia's perl.prov improvements back into upstream
  o merge mageia's find-requires.sh improvements back into upstream

* Fri Sep 05 2014 Thierry Vignaud <tv@mageia.org> 1:4.11.3-1.mga5
+ Revision: 672469
- adjust file list
- new release
- fix building with plugins
- drop commented out patch (which is implemented differently in 4.12 anyway)
- drop "Requires: glibc >= 2.1.92" (mga has newer & glibc is autorequired)

* Sun Jul 27 2014 Colin Guthrie <colin@mageia.org> 1:4.11.2-7.mga5
+ Revision: 657571
- Implement support for prioritised filetriggers

* Tue Jul 01 2014 Thierry Vignaud <tv@mageia.org> 1:4.11.2-6.mga5
+ Revision: 641727
- require fixed lua-5.2

* Mon Jun 30 2014 Thierry Vignaud <tv@mageia.org> 1:4.11.2-5.mga5
+ Revision: 641668
- build with lua-5.2
- speedup testsuite by using eatmydata
+ Pascal Terjan <pterjan@mageia.org>
- Rebuild for new Python

* Thu May 08 2014 D Morgan <dmorgan@mageia.org> 1:4.11.2-3.mga5
+ Revision: 621327
- reduce the double separator spec parse error into a warning (RHBZ #1065563)

* Wed Feb 19 2014 Thierry Vignaud <tv@mageia.org> 1:4.11.2-2.mga5
+ Revision: 594917
- submit to core/release

* Fri Feb 14 2014 Thierry Vignaud <tv@mageia.org> 1:4.11.2-1.mga5
+ Revision: 591324
- new release

* Thu Feb 06 2014 Thierry Vignaud <tv@mageia.org> 1:4.11.2-0.rc2.8.mga5
+ Revision: 584830
- new RC

* Tue Feb 04 2014 Thierry Vignaud <tv@mageia.org> 1:4.11.2-0.rc1.8.mga5
+ Revision: 581966
- 4.11.2~rc1
- rediff patch 3000

* Fri Nov 15 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.1-8.mga4
+ Revision: 551382
- submit to core/release

* Wed Nov 13 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.1-7.mga4
+ Revision: 550967
- drop "fix aborting when ordering empty transactions" patch
  (rpm was fixed and perl-URPM-4.22+ no more create them anyway)
- fix yum segfaulting (mga#11621, Panu)

* Tue Oct 22 2013 Sysadmin Bot <umeabot@mageia.org> 1:4.11.1-6.mga4
+ Revision: 546201
- Mageia 4 Mass Rebuild

* Thu Oct 17 2013 Luc Menut <lmenut@mageia.org> 1:4.11.1-5.mga4
+ Revision: 502275
- patch 4008: don't bytecompile python files in docdir

* Wed Oct 02 2013 Colin Guthrie <colin@mageia.org> 1:4.11.1-4.mga4
+ Revision: 490444
- Disable debuginfo extraction 'fix' as it breaks more than it fixes
- Do not use %%attr on symlinks

* Sat Sep 28 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.1-3.mga4
+ Revision: 488504
- sync weak deps with suse now that they've updated to 4.11.0 too (mga#11095)
+ Colin Guthrie <colin@mageia.org>
- Fix debuginfo extraction on some configurations

* Thu Aug 22 2013 Kamil Rytarowski <kamil@mageia.org> 1:4.11.1-2.mga4
+ Revision: 469312
- be more generic with hardcoding linux specific configuration in the .spec

* Thu Jun 27 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.1-1.mga4
+ Revision: 447560
- new release

* Thu Jun 20 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.1-0.rc2.1.mga4
+ Revision: 445226
- RC2

* Tue Jun 11 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.1-0.rc1.3.mga4
+ Revision: 441933
- patch 1000: fix an upstream regression
- adapt to upstreap isNewDep() changes
- display testsuite errors when those happen
- fix testsuite regressions

* Mon Jun 10 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.1-0.rc1.2.mga4
+ Revision: 441839
- restore lost patch bits

* Mon Jun 10 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.1-0.rc1.1.mga4
+ Revision: 441834
- adjust file list
- new release

* Sun Feb 03 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.0.1-1.mga3
+ Revision: 394238
- reset mkrel

* Fri Feb 01 2013 Thierry Vignaud <tv@mageia.org> 1:4.11.0.1-9.mga3
+ Revision: 393936
- new release
- drop patch 4004
- rediff patch 3500

* Tue Jan 22 2013 Funda Wang <fwang@mageia.org> 1:4.11.0-0.beta1.9.mga3
+ Revision: 390713
- update rpm group

* Sun Jan 13 2013 Sysadmin Bot <umeabot@mageia.org> 1:4.11.0-0.beta1.8.mga3
+ Revision: 380447
- Mass Rebuild - https://wiki.mageia.org/en/Feature:Mageia3MassRebuild

* Mon Dec 31 2012 Thierry Vignaud <tv@mageia.org> 1:4.11.0-0.beta1.7.mga3
+ Revision: 337028
- switch debug packages from foobar-debug to foobar-debuginfo

* Sun Dec 23 2012 Thierry Vignaud <tv@mageia.org> 1:4.11.0-0.beta1.6.mga3
+ Revision: 334395
- submit to core/release

* Tue Dec 18 2012 Thierry Vignaud <tv@mageia.org> 1:4.11.0-0.beta1.5.mga3
+ Revision: 332644
- restore check for /usr move (was wrongly removed)

* Tue Dec 18 2012 Thierry Vignaud <tv@mageia.org> 1:4.11.0-0.beta1.4.mga3
+ Revision: 332323
- fix abort while ordering empty transactions

* Mon Dec 17 2012 Thierry Vignaud <tv@mageia.org> 1:4.11.0-0.beta1.3.mga3
+ Revision: 332188
- revert upstream debug -> debuginfo rename

* Mon Dec 17 2012 Thierry Vignaud <tv@mageia.org> 1:4.11.0-0.beta1.2.mga3
+ Revision: 332169
- patch 83: plug memory leaks

* Mon Dec 17 2012 Thierry Vignaud <tv@mageia.org> 1:4.11.0-0.beta1.1.mga3
+ Revision: 332065
- new release
- drop merged patches 3001 3500 4001 4002 & 4003
- rediff patches 83 & 145

* Sun Dec 09 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.2-1.mga3
+ Revision: 329045
- patch 4003: build fix
- new release
- drop patches 3503 & 4003 (merged)
- rediff patch 3002

* Thu Dec 06 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.1-6.mga3
+ Revision: 327271
- unbreak --setperms (RhBug:881835) (regression introduced in rpm >= 4.10
- make traversing rpmdb through NEVRA more robust

* Sat Dec 01 2012 Jani Välimaa <wally@mageia.org> 1:4.10.1-5.mga3
+ Revision: 324218
- restore and rediff patch 157 (introduce-_after_setup-which-is-called-after-setup.patch), calling rpmbuild or bm with option '--with git_repository' is broken without this patch

* Thu Oct 18 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.1-4.mga3
+ Revision: 308048
- enhance debugedit message

* Wed Oct 17 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.1-3.mga3
+ Revision: 307768
- upload to core/release

* Thu Oct 11 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.1-2.mga3
+ Revision: 304675
- patch 5303: fix hardlink checking regression

* Wed Oct 03 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.1-1.mga3
+ Revision: 302329
- new release
- rediff patch 3000
- requires xz instead of lzma

* Tue Aug 28 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-11.mga3
+ Revision: 284979
- rebuild for new binutils

* Sat Aug 04 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-10.mga3
+ Revision: 278429
- rebuild with compressed debug info
- re-enable testsuite

* Thu Aug 02 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-9.mga3
+ Revision: 277894
- rebuild with mini debug info

* Tue Jul 31 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-8.mga3
+ Revision: 276795
- patch 4000: merge find-debuginfo.sh changes from rpm-mageia-setup
- temporary disable test suite as the full build takes 5mn locally & on 32bits
  BS but 2 hours on jonud 64bit

* Tue Jul 31 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-7.mga3
+ Revision: 276745
- add "compressed" debug packages support

* Mon Jul 30 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-6.mga3
+ Revision: 276213
- add minidebug support
- build with db-5.3

* Thu Jul 19 2012 Colin Guthrie <colin@mageia.org> 1:4.10.0-5.mga3
+ Revision: 272737
- Add Fedora patch to add a new filesystem check for the usrmove

* Sun Jul 08 2012 D Morgan <dmorgan@mageia.org> 1:4.10.0-4.mga3
+ Revision: 268937
- Update rubygem patch ( from mandriva )

* Wed Jul 04 2012 Arnaud Patard <rtp@mageia.org> 1:4.10.0-3.mga3
+ Revision: 267694
- fix testsuite
- fix filelist

* Wed Jun 27 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-2.mga3
+ Revision: 264615
- patch 133: fix segfaulting in "rpm -q --suggests foobar"

* Tue Jun 26 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-1.mga3
+ Revision: 264064
- fix & reenable testsuite

* Wed Jun 20 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-0.8.mga3
+ Revision: 262322
- drop patches:
  o 137, as there's no guaranty on the actual current encoding, we may
    return garbage and anyway mga has no l10n specs
  o 152: nss is now set at configure time
  o 1009 (rpm5 tags) as rpm-4.9+ doesn't choke on them
  o 2005, obsolete as rpm-4.8+ set LANG in %%___build_pre
- patch 2100: fix running testsuite when not having the uncompiled "magic" file
- rpmsign is only in rpmsign, not in main package too

* Tue Jun 12 2012 Funda Wang <fwang@mageia.org> 1:4.10.0-0.3.mga3
+ Revision: 260027
- add conflicts on jpackage to ease upgarde

* Fri Jun 08 2012 Thierry Vignaud <tv@mageia.org> 1:4.10.0-0.2.mga3
+ Revision: 257999
- fix filelist on ia32
- temporary disable testsuite due to an issue with the 'file' command
- bump conflicts with perl-URPM
- switch from db4.8 to db5.2
- patch 83: fix regression on doc conflict check; also reduces check overhead a little bit
- BR lua5.1-devel instead of lua-devel
- rediff patches 3002-4 (MIPS & ARM)
- annotate some patches
- drop (very) old post script for convertrpmr.sh
- rediff patches 3002-4 (MIPS & ARM)
- rediff patches 83 111 159 1009 2006
- patch 49 was merged upstream
- new release
- rediff weakdep patch
- drop merged FC patches
- drop merged patch 4000
- temporary disable patches 49, 83, 111, 145, 159, 1009, 2006

* Thu Apr 19 2012 D Morgan <dmorgan@mageia.org> 1:4.9.1.3-2.mga2
+ Revision: 231844
- Add upstream patch Warn but dont fail the build on STABS debuginfo (RhBug:725378, others)

* Wed Apr 04 2012 Thierry Vignaud <tv@mageia.org> 1:4.9.1.3-1.mga2
+ Revision: 228572
- new release (pure security fixes)

* Tue Mar 13 2012 Thierry Vignaud <tv@mageia.org> 1:4.9.1.2-22.mga2
+ Revision: 223263
- BR fakechroot, thus fixing all remaining testsuite issues (but a rpmspec
  faillure due to /etc/rpm/macros.d/20build.macros)

* Tue Mar 13 2012 Thierry Vignaud <tv@mageia.org> 1:4.9.1.2-21.mga2
+ Revision: 223206
- merge patches 4103-4108 from FC:
  o fix memory corruption on rpmdb size estimation (#766260)
    (might help mga#4918):
  o fix couple of memleaks in python bindings (RH#782147)
  o fix regression in verify output formatting (RH#797964)
  o dont process spec include in false branch of if (RH#782970)
  o only warn on missing excluded files on build (RH#745629)
  o dont free up file info sets on test transactions
- patches 133, 1009: fix a testsuite regression

* Sat Mar 03 2012 Thomas Backlund <tmb@mageia.org> 1:4.9.1.2-20.mga2
+ Revision: 217265
- restore P4000 as it's not causing the the out of memory errors

* Fri Mar 02 2012 D Morgan <dmorgan@mageia.org> 1:4.9.1.2-19.mga2
+ Revision: 216922
- Comment P4000, just to see if the regression saw is because of this

* Tue Feb 28 2012 Thierry Vignaud <tv@mageia.org> 1:4.9.1.2-18.mga2
+ Revision: 215854
- simplify patch 70
- revert changes to patch 133, instead include missing extcond patch 134 (SuSE, mga#4012)
+ Funda Wang <fwang@mageia.org>
- use configure2_5x

* Mon Feb 27 2012 Thierry Vignaud <tv@mageia.org> 1:4.9.1.2-17.mga2
+ Revision: 215717
- fix rpm -q --suggests (mga#4012)
- patch 5000: switch back to former, much smaller BDB memory pool size (RhBug:752897)

* Tue Feb 07 2012 Thierry Vignaud <tv@mageia.org> 1:4.9.1.2-16.mga2
+ Revision: 206214
- run testsuite

* Thu Jan 12 2012 Thierry Vignaud <tv@mageia.org> 1:4.9.1.2-15.mga2
+ Revision: 195417
- patch 3004: unconditionally disable libio usage (mga#1962)
+ D Morgan <dmorgan@mageia.org>
- Fix ARM support ( rtp)
- Add back P3004: Fix ARM support

* Sat Dec 24 2011 D Morgan <dmorgan@mageia.org> 1:4.9.1.2-14.mga2
+ Revision: 187127
- Fix perms of script.req and elfdeps

* Fri Dec 23 2011 Anssi Hannula <anssi@mageia.org> 1:4.9.1.2-13.mga2
+ Revision: 186888
- fix filetriggers that were broken by incomplete rediff of the patch for
  rpm 4.9 (filetriggers.patch)
+ D Morgan <dmorgan@mageia.org>
- Remove P5000, not usefull for us now

* Wed Dec 21 2011 D Morgan <dmorgan@mageia.org> 1:4.9.1.2-12.mga2
+ Revision: 185220
- Add %%epoch in Requires ( tks to boklm)

* Wed Dec 21 2011 D Morgan <dmorgan@mageia.org> 1:4.9.1.2-11.mga2
+ Revision: 185191
- Fix requires of the devel package

* Tue Dec 20 2011 Olivier Blin <blino@mageia.org> 1:4.9.1.2-10.mga2
+ Revision: 185127
- fix perms of fileattrs directory (seems to break pkgconfig auto provides/requires)

* Tue Dec 20 2011 D Morgan <dmorgan@mageia.org> 1:4.9.1.2-9.mga2
+ Revision: 185086
- Rebuild in the BS
- Update to rpm 4.9.1.2 ( from work done on branch)
- Remove old sources

* Mon Nov 21 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-15.mga2
+ Revision: 170213
- Remove .la files

* Wed Nov 16 2011 Jerome Quelin <jquelin@mageia.org> 1:4.8.1-14.mga2
+ Revision: 168128
- another perl module required - this time for module::install powered dists

* Tue Nov 15 2011 Jerome Quelin <jquelin@mageia.org> 1:4.8.1-13.mga2
+ Revision: 167931
- perl(eumm) needs a recent CPAN::Meta to generate MYMETA files

* Mon Nov 14 2011 Jerome Quelin <jquelin@mageia.org> 1:4.8.1-12.mga2
+ Revision: 167751
- require a recent EUMM for perl in order to generate MYMETA files

* Sun Jun 19 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-11.mga2
+ Revision: 110107
- Add  File capability support

* Mon May 16 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-10.mga1
+ Revision: 99198
- Fix P147 to check for mageia too ( spotted by tvignaud )

* Sun Mar 27 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-9.mga1
+ Revision: 78198
- Fix previous commit
- Backport mdv commit 535522
  	- fix file trigger hang when several filetrigger scripts are run in parallel (mdvbz #57878)

* Fri Mar 18 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-8.mga1
+ Revision: 74203
- Add patch from rtp to fix build on arm (P3005)

* Fri Mar 04 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-7.mga1
+ Revision: 64225
- Really fix filetriggers

* Fri Mar 04 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-5.mga1
+ Revision: 64206
- Use texlive instead of tetex
- Fix file triggers

* Wed Mar 02 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-4.mga1
+ Revision: 62722
- Add a comment about patch64
- Add a comment about patch31 sent upstream
- As patch17 have been previously dropped, remove it from the spec file
- Change syslog patch and use the one sent upstream

* Tue Mar 01 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-3.mga1
+ Revision: 62080
- Drop old patch17
  Signed-off-by: cfergeau
  Signed-off-by: tvignaud

* Tue Mar 01 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-2.mga1
+ Revision: 62040
- Readd rpm-4.8.1-setup-rubygems.patch ( candidate for upstream )

* Thu Feb 24 2011 D Morgan <dmorgan@mageia.org> 1:4.8.1-1.mga1
+ Revision: 59113
- Add comment about patch17 that is a candidate for a drop
- Add exit to not build rpm for now
- Fix spec file ( from tv work)
- Comment release to be sure we do not upload rpm w/o a working perl-RPM4
- Remove patch 1017, to not diverge from main rpm.org regarding version comparison
- Restore rpm.org behaviour for %%exclude
- Update to rpm 4.8.1
- Fix typo
- Update to rpm 4.8 ( from cfergeau work )
+ Thomas Backlund <tmb@mageia.org>
- read rpmrc and rpmpopt from /usr/lib/rpm/mageia/
+ Olivier Blin <blino@mageia.org>
- add back manbo-rpmrc patch in SOURCES (thanks rtp)
- use _real_vendor macro
- revert manbo drop, it breaks many things (and builds packages as i386 on i586), we need to merge manbo files instead of dropping them

* Wed Jan 19 2011 Thomas Backlund <tmb@mageia.org> 1:4.6.1-9.mga1
+ Revision: 24931
- hardcode mageia as distro name for now in order to force dropping requirements on rpm-manbo-setup-*
- drop P64, as it's manbo specific

* Mon Jan 17 2011 Olivier Blin <blino@mageia.org> 1:4.6.1-8.mga1
+ Revision: 20921
- rebuild with new python

* Sun Jan 09 2011 Olivier Blin <blino@mageia.org> 1:4.6.1-7.mga1
+ Revision: 3665
- rebuild with rpm-mageia-setup

* Sun Jan 09 2011 Olivier Blin <blino@mageia.org> 1:4.6.1-6.mga1
+ Revision: 3615
- put the lowercase distro name in a macro (mandriva for now, as long as
  rpm-mageia-setup is not available, which needs an updated rpm)
- rebuild with rpm-mandriva-setup for now
- update rpmbuild unpackaged files check for mga
+ Thomas Backlund <tmb@mageia.org>
- drop rpm tags
- rename mandriva to mageia
- remove support for old mandriva versions
- remove vendor checks (was only needed for Manbo labs)
- imported package rpm
